package com.example.servingwebcontent.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Table("keys")
public class KeyModel {
    @Id
    @Column("id")
    private Long id;

    @Column("key")
    private String key;
}
