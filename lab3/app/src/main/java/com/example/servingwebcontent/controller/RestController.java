package com.example.servingwebcontent.controller;

import com.example.servingwebcontent.dao.KeyRepository;
import com.example.servingwebcontent.model.KeyModel;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;

@RequiredArgsConstructor
@org.springframework.web.bind.annotation.RestController
public class RestController {

    private final KeyRepository keyRepository;

    @GetMapping("/db")
    public Iterable<KeyModel> getStrings() {
        return keyRepository.findAll();
    }
}
