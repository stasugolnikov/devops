#!/bin/zsh

# Подробные логи выполняемых команд
set -x

# Не продолжать выполнение команд при ошибке
set -e

# Сборка образа для системных зависимостей приложения
docker build \
    --target system \
    --cache-from bigfirestart/lab-5:system \
    -t bigfirestart/lab-5:system \
    -f Dockerfile \
    .

# Сборка образа для сборки приложения
docker build \
    --target build \
    --cache-from bigfirestart/lab-5:system \
    --cache-from bigfirestart/lab-5:build \
    -t bigfirestart/lab-5:build \
    -f Dockerfile \
    .

# Сборка приложения
docker build \
    --target app \
    --cache-from bigfirestart/lab-5:build \
    -t bigfirestart/lab-5:app \
    -f Dockerfile \
    .

# ================================================

# Сборка приложения при помощи кэширующего бэкенда

# Примечание: в качестве кэша используется docker registry
# Собранный кэш можно увидеть по ссылке: https://hub.docker.com/repository/docker/bigfirestart/lab-5/general


# Создание локального билдера с кэшем
# -----------------------------------
# docker buildx create --use --driver=docker-container
# > wonderful_sinoussi

# Или используем созданный
# ------------------------
# docker buildx use wonderful_sinoussi

# docker buildx build \
#     --target system \
#     --cache-from bigfirestart/lab-5:system \
#     --cache-to bigfirestart/lab-5:system \
#     -t bigfirestart/lab-5:system \
#     -f Dockerfile \
#     .

# docker buildx build \
#     --target build \
#     --cache-from bigfirestart/lab-5:system \
#     --cache-from bigfirestart/lab-5:build \
#     --cache-to bigfirestart/lab-5:build \
#     -t bigfirestart/lab-5:build \
#     -f Dockerfile \
#     .

# docker buildx build \
#     --target app \
#     --cache-from bigfirestart/lab-5:build \
#     --cache-to bigfirestart/lab-5:app \
#     -t bigfirestart/lab-5:app \
#     -f Dockerfile \
#     .
