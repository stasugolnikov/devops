ARG SYSTEM_IMAGE=docker.io/arm64v8/gradle:7.6.0-jdk19-jammy

FROM ${SYSTEM_IMAGE} AS system

# Note: потенциально можно было бы: FROM alpine => RUN apk add <...> => etc.

# Note: для используемого образа дополнительных зависимостей устанавливать не нужно

# Системные зависимости находятся по следующим путям:

# /opt/java/openjdk     # <-- Тут находятся все необходимые файлы для java
# /lib                  # <-- Тут находятся динамически загружаемые библиотеки для бинарного файла java

# Вывод команды ldd для java:
# > # ldd java
# 	linux-vdso.so.1 (0x0000ffffa74b1000)
# 	libz.so.1 => /lib/aarch64-linux-gnu/libz.so.1 (0x0000ffffa7420000)
# 	libjli.so => /opt/java/openjdk/bin/./../lib/libjli.so (0x0000ffffa73f0000)
# 	libpthread.so.0 => /lib/aarch64-linux-gnu/libpthread.so.0 (0x0000ffffa73d0000)
# 	libdl.so.2 => /lib/aarch64-linux-gnu/libdl.so.2 (0x0000ffffa73b0000)
# 	libc.so.6 => /lib/aarch64-linux-gnu/libc.so.6 (0x0000ffffa7200000)
# 	/lib/ld-linux-aarch64.so.1 (0x0000ffffa7478000)

# ---

FROM system AS build
WORKDIR /app

ENV GRADLE_USER_HOME=/app/gradle

COPY ./app/settings.gradle ./app/build.gradle ./
RUN gradle :dependencies

COPY ./app/ .
RUN gradle :bootJar

# ---

FROM scratch AS app
WORKDIR /app

# Системные зависимости
COPY --from=build /opt/java/openjdk /opt/java/openjdk
COPY --from=build /lib /lib

# Зависимости сборки
COPY --from=build /app/build/libs/*.jar ./app.jar

ENTRYPOINT ["/opt/java/openjdk/bin/java"]
CMD ["-jar", "app.jar"]
