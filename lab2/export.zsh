#!/bin/zsh

cd $(dirname $0)

if [[ ! -d tmp ]]; then
    mkdir tmp
    echo '*' > .gitignore
fi

echo $(
    docker run -v lab_certificates:/cert -it --rm arm64v8/alpine cat /cert/server.crt
) > tmp/server.crt
