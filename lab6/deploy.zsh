#!/bin/zsh

# Подробные логи выполняемых команд
set -x

# Не продолжать выполнение команд при ошибке
set -e

INIT_PWD=$(pwd)

# Запуск
PROJECTS='db backend frontend nginx-proxy'
for project in $(echo $PROJECTS); do
    cd ${project}
    docker compose up -d
    cd ${INIT_PWD}
done
