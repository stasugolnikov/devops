#!/bin/zsh

cd $(dirname $0)

if [[ ! -d _tmp ]]; then
    mkdir _tmp
    cd _tmp
    echo '*' > .gitignore
    cd ..
fi

echo $(
    docker run -v lab-6-nginx-proxy_certificates:/cert -it --rm arm64v8/alpine cat /cert/server.crt
) > _tmp/server.crt
