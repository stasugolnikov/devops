#!/bin/zsh
set -xe

INIT_PWD=$(pwd)
PROJECTS='nginx-proxy frontend backend db'
for project in $(echo $PROJECTS); do
    cd ${project}
    docker compose down
    cd ${INIT_PWD}
done
